object MyModule {
	def fib(n: Int): Int = {
		if (n == 0 || n == 1) n
		else fib(n-1) + fib(n-2)
	}

	def fibTail(n: Int): Int = {
		@annotation.tailrec
		def go(n: Int, a: Int, b: Int): Int = {
			if (n == 0) a
			else go(n - 1, b, a + b)
		}

		go(n, 0 ,1)
	}

	def abs(n: Int): Int = 
		if (n < 0) -n 
		else  n 

	def factorial(n: Int): Int = {
		@annotation.tailrec
		def go(n: Int, acc: Int): Int = 
			if (n <= 0)	acc
			else go(n-1, n*acc)

		go(n, 1)
	}	

	private def formatAbs(x: Int) = {
	 	val msg = "The absolute value of %d is %d"
	 	msg.format(x, abs(x))
	} 

	private def formatFactorial(x: Int) = {
	 	val msg = "The factorial of %d is %d"
	 	msg.format(x, factorial(x))
	}

	private def formatResult(name: String, n: Int, f: Int => Int) = {
		val msg = "The %s of %d is %d"
		msg.format(name, n, f(n))
	}

	def findFirst(ss: Array[String], key: String): Int = {
		@annotation.tailrec
		def loop(n: Int): Int = {
			if (n >= ss.length) -1
			else if (ss(n) == key) n
			else loop(n + 1)
		}
		loop(0)
	}

	//Using MyModule.findFirstGeneric[String](arr, x => x == "D")
	def findFirstGeneric[T](ss: Array[T], p: T => Boolean): Int = {
		@annotation.tailrec
		def loop(n: Int): Int = {
			if (n >= ss.length) -1
			else if (p(ss(n))) n
			else loop(n + 1)
		}
		loop(0)
	}

	def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = {
		@annotation.tailrec
		def loop(n: Int): Boolean = {
			if (n >= as.length - 1) true
			else !ordered(as(n), as(n + 1)) && loop(n + 1)
		}
		loop(0)
	}

	//Polymorphic fun
	def partial1[A,B,C](a: A, f: (A, B) => C): B => C = (b: B) => f(a, b)

	//Ex3 curring
	def curry[A,B,C](f: (A,B) => C): A => (B => C) = a => b => f(a, b)

	//Ex4 uncurring
	def uncurry[A,B,C](f: (A,B) => C): (A, B) => C = (a, b) => f(a)(b) 

	//Ex5 composion
	def compose[A,B,C](f: B => C, g: A => B): A => C = a => f(g(a))

 
	def main(args: Array[String]): Unit = {
		println(formatResult("absolute value", -43, abs))
		println(formatResult("factorial", 12, factorial))
	}
}