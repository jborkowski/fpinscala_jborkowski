sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A] (head: A, tail: List[A]) extends List[A]

object List {
	def sum(inits: List[Int]): Int = inits match {
		case Nil => 0
		case Cons(x, xs) => x + sum(xs) 
	}

	def product(ds: List[Double]): Double = ds match {
		case Nil => 1.0
		case Cons(0.0, _) => 0.0
		case Cons(x, xs) => x * product(xs)
	}
	 
	def apply[A](as: A*): List[A] = {
		if (as.isEmpty) Nil
		else Cons(as.head, apply(as.tail: _*))
	}

	def tail[A](ls: List[A]) = ls match {
		case Nil => Nil
		case Cons(_, xs) => xs
	}

	def setHead[A](ls: List[A], rv: A): List[A] = Cons(rv, tail(ls))
	
	def setHead1[A](l: List[A], n: A): List[A] = l match {
		case Nil => sys.error("setHead o nempty list")
		case Cons(_, x) => Cons(n, x) 
	}

	def drop[A](l: List[A], n: Int): List[A] = {
		if (n <= 0) l
		else l match {
			case Nil => Nil
			case Cons(_, x) => drop(x, n-1)
		}	
	}	

	def dropWhile[A](l: List[A], fn: A => Boolean): List[A] = l match {
		case Cons(x, xs) if fn(x) => dropWhile(xs, fn)
		case _ => l
	}

	def init[A](l: List[A]): List[A] = l match {
		case Nil => sys.error("emptylist")
		case Cons(_, Nil) => Nil
		case Cons(head, tail) => Cons(head, init(tail))
	}


	def foldRight[A, B](ls: List[A], z: B)(f: (A, B) => B): B = ls match {
		case Nil => z
		case Cons(x, xs) => f(x, foldRight(xs, z)(f)) 
	}
	
	def sum2(ls: List[Int]) = foldRight(ls, 0)(_ + _)

	def product2(ls: List[Double]) = foldRight(ls, 1.0)(_ * _)

	def length[A](ls: List[A]): Int = foldRight(ls, 0)((_, acc) => acc + 1)
	
	def foldLeft[A,B](ls: List[A], z: B)(f: (B, A) => B): B = {
		@annotation.tailrec
		def go(l: List[A], acc: B): B = l match {
			case Nil => acc
			case Cons(x, xs) => go(xs, f(acc, x)) 
		}
		go(ls, z)
	}

	def sum3(l: List[Int]) = foldLeft(l, 0)(_ + _)

	def product3(l: List[Int]) = foldLeft(l, 0)(_ * _)

	def length1[A](l: List[A]) = foldLeft(l, 0)((acc, _) => acc + 1)

	def reverse[A](l: List[A]): List[A] = foldLeft(l, List[A]())((acc, h) => Cons(h, acc))

	def append[A](l1: List[A], l2: List[A]) = foldRight(l1, l2)(Cons(_, _))
		
	
}