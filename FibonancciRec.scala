object FibonancciRec {
	def fib(n: Int): Int = {
		if (n == 0 || n == 1) n
		else fib(n-1) + fib(n-2)
	}

	def fibTail(n: Int): Int = {
		@annotation.tailrec
		def go(n: Int, a: Int, b: Int): Int = {
			if (n == 0) a
			else go(n - 1, b, a + b)
		}

		go(n, 0 ,1)
	}
}